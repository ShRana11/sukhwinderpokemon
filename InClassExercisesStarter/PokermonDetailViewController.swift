//
//  PokermonDetailViewController.swift
//  InClassExercisesStarter
//
//  Created by Sukhwinder Rana on 2018-12-02.
//  Copyright © 2018 room1. All rights reserved.
//

import UIKit
import FirebaseFirestore
import Alamofire
import SwiftyJSON
import CoreLocation
import  MapKit


class PokermonDetailViewController:UIViewController,CLLocationManagerDelegate  {
    var manager:CLLocationManager!
    var username = ""
    var latitude = 43.6532
    var longitude = -79.3832
    var l = 0.0001
    //set value of l into user defaults
    var db:Firestore!
    var pokemonName = ""
    var rowImage = ""
    var r = 0
    //var items = ["Mew", "Pikachu", "Squirtle","Zubar"]
    
    @IBOutlet weak var labelSucessMessage: UILabel!
    @IBOutlet weak var pokemonDetailLabel: UILabel!
    @IBOutlet weak var pokemonImage: UIImageView!
    //@IBOutlet weak var lblResult: UILabel!
    // Mark: Firestore variables
    var bc = ""
    @IBAction func buttonSelectPokemon(_ sender: Any) {
        db = Firestore.firestore()
//        let ref = db.collection("users").whereField("name", isEqualTo: self.username)
//        ref.getDocuments() {
//            (querySnapshot, err) in
//            if (err == nil){
//                for document in querySnapshot!.documents {
//                    print("\(document.documentID) => \(document.data())")
//                    //self.labelSucessMessage.text! = "\(document.data())"
//
//                    self.pokemonImage.image = UIImage(named: self.rowImage)
//                }
//            }
//            else if let err = err {
//                print("this restauran is not in database")
//                self.labelSucessMessage.text! = "Error getting documents: \(err)"
//            }
//        }
        UserData()
    }
    // MARK: Default Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        //let name = self.name
        db = Firestore.firestore()
        if UserDefaults.standard.object(forKey: "lat") != nil
        {
            latitude =  Double(UserDefaults.standard.string(forKey: "lat")!) ?? 43.6532
            longitude =  Double(UserDefaults.standard.string(forKey: "lng")!) ?? -79.3832
        }
        manager = CLLocationManager()
        manager.delegate = self
        
        // how accurate do you want the lkocation to be?
        manager.desiredAccuracy = kCLLocationAccuracyBest
        
        // ask for permission to get the location
        manager.requestAlwaysAuthorization()
        
        // tell the manager to get the person's location
        manager.startUpdatingLocation()
        let ref = db.collection("Pokemon").whereField("name", isEqualTo: self.pokemonName)
        ref.getDocuments() {
            (querySnapshot, err) in
            if (err == nil){
                for document in querySnapshot!.documents {
                    print("\(document.documentID) => \(document.data())")
                    self.labelSucessMessage.text! = "\(document.data())"
                    print(self.rowImage)
                    self.pokemonImage.image = UIImage(named: self.rowImage)
                }
            }
            else if let err = err {
                print("this Pokemon is not in database")
                self.labelSucessMessage.text! = "Error getting documents: \(err)"
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("got a new location")
        
        if (locations.count == 0) {
            print("Error getting your location!")
            return
        }
        else {
            print(locations[0])
            let searchRequest = MKLocalSearchRequest()
            latitude = locations[0].coordinate.latitude
            longitude = locations[0].coordinate.longitude
            
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func  UserData() {
        let user = db.collection("users")
        //let pokemon = db.collection("Pokemon").document("Meon")
        
        //        user.document("jenelle@gmail.com").setData([
        //            "name": "jenelle@gmail.com",
        //            "latitude": 43.6532 ,
        //            "longitude": -79.3832,
        //            "pokemon": "Meon"
        //            ])
        latitude = latitude + l
        longitude = longitude - l
        let userDefaults = UserDefaults.standard
        userDefaults.setValue(latitude, forKey: "lat")
        userDefaults.setValue(longitude, forKey: "lng")
        userDefaults.synchronize()
        var n = self.username
        user.document(n).setData([
            "name": self.username,
            "latitude": latitude,
            "longitude": longitude,
            "pokemon": self.pokemonName ?? "Meon",
            "icon": self.rowImage
            ])
    }
    

   
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        let n1 = segue.destination as! PokemonMapViewController
        n1.pokemonName = self.pokemonName
        n1.username = self.username
        n1.row = self.rowImage
    }
   

}
